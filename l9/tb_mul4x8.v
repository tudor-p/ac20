module tb_mul4x8;

reg clk;
reg res;
reg [3:0] a;
reg [7:0] b;
wire [11:0] p;

integer r0, r1, r2, r3;
integer clk_cnt;

mul4x8 DUT(
	clk,
	res,
	a,
	b,
	p
);

always #5 clk = ~clk;

always@(posedge clk) begin
	r0<=a*b; r1<=r0;	r2<=r1;	r3<=r2;
	if(res) clk_cnt<=0;
	else clk_cnt<=clk_cnt + 1;
	if(clk_cnt >= 3 && r3 !== p) begin
		$display("expected %d, actual %d", r3, p);
		$finish();
	end
end

initial begin
	$dumpfile("mul4x8.vcd");
	$dumpvars;
	
	clk = 0;
	res = 1;
	a = 0; b = 0;
	
	#10 res = 0;

	#10 a = 11; b = 9;
	#10 a = 12; b = 8;
	#10 a = 13; b = 11;	
	#10 a = 14; b = 60;
	#10 a = 15; b = 100;
	#10 a = 12; b = 252;
	
	#50 $finish();
end

endmodule
