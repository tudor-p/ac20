
module mul(
	input clk,
	input res,
	input [3:0] a,
	input [3:0] b,
	output [7:0] p
);

reg [3:0] rp1;
reg [5:0] rp2;
reg [6:0] rp3;
reg [7:0] rp4;

reg [3:0] a1;
reg [3:0] a2;
reg [3:0] a3;

reg [3:1] b1;
reg [3:2] b2;
reg [3:3] b3;

assign p = rp4;

always@(posedge clk) begin
	if (res == 1) begin
		a1 <= 0; a2 <= 0; a3 <= 0;
		b1 <= 0; b2 <= 0; b3 <= 0;
	end else begin
		a1 <= a; a2 <= a1; a3 <= a2;
		b1 <= b[3:1]; b2 <= b1[3:2]; b3 <= b2[3:3];
	end
end
	
always@(posedge clk) begin
	if (res == 1) 
		rp1 <= 0;
	else
		rp1 <= a & {4{b[0]}};
end

always@(posedge clk) begin
	if (res == 1) 
		rp2 <= 0;
	else begin
		//!!// rp2 <= {{a1 & {4{b1[1]}}} + {1'b0, {rp1[3:1]}}, rp1[0]};
		rp2[5:1] <= {a1 & {4{b1[1]}}} + {1'b0, {rp1[3:1]}};
		rp2[0] 	 <= rp1[0];
	end
end

always@(posedge clk) begin
	if (res == 1) 
		rp3 <= 0;
	else begin
		rp3[6:2] <= {a2 & {4{b2[2]}}} + {rp2[5:2]};
		rp3[1:0] <= rp2[1:0];
	end
end

always@(posedge clk) begin
	if (res == 1) 
		rp4 <= 0;
	else begin
		rp4[7:3] <= {a3 & {4{b3[3]}}} + {rp3[6:3]};
		rp4[2:0] <= rp3[2:0];
	end
end

endmodule
