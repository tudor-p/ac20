module mul4x8(
	input clk,
	input res,
	input [3:0] a,
	input [7:0] b,
	output [11:0] p
);

wire [7:0] pp11_4, pp7_0;

// a[3:0] * b[7:0] = a[3:0] * (b[7:4] * 16 + b[3:0]) 
// = a[3:0] * b[7:4] * 16 + a[3:0] * b[3:0]

mul m0(clk, res, a[3:0], b[7:4], pp11_4);

mul m1(clk, res, a[3:0], b[3:0], pp7_0);

// 
assign p = {pp11_4, 4'b0000} + pp7_0; 

endmodule
