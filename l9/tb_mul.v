module tb_mul;

reg clk;
reg res;
reg [3:0] a;
reg [3:0] b;
wire [7:0] p;

mul DUT(
	clk,
	res,
	a,
	b,
	p
);

always #5 clk = ~clk;

initial begin
	$dumpfile("mul.vcd");
	$dumpvars;
	
	clk = 0;
	res = 1;
	a = 0; b = 0;
	
	#10 res = 0;


	#10 a = 5; b = 9;
	#10 a = 8; b = 10;

	#10 a = 11; b = 9;
	#10 a = 12; b = 8;
	#10 a = 13; b = 11;	

	#50 $finish();
end

endmodule
