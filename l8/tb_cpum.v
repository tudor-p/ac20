module tb_cpum;

reg clk;
reg res;
integer i;

cpum DUT(
	clk,
	res
);

initial begin
	$dumpfile("cpum.vcd");
	$dumpvars(0, DUT);

	// view some memory locations
	for(i = 0; i < 12; i = i + 1)
		$dumpvars(1, DUT.DP.mem[i]);

	for(i = 4*64; i < 4*70; i = i + 1)
		$dumpvars(1, DUT.DP.mem[i]);
	
	// view registers
	for(i = 0; i < 32; i = i + 1)
		$dumpvars(1, DUT.DP.regs[i]);	
	
	#0
	clk = 0; res = 1;
	#10 
	res = 0;
	#1000
	$finish();
end

always #5 clk = ~clk;

endmodule
