
module ctrl_fsm(
	input clk,
	input res,
	
	input [5:0] op_code,
	input overflow,
	output reg [18:0] control
);

reg [3:0] state_crt, state_nxt;

// compute state_nxt
always @(state_crt or op_code)
	casex ({state_crt, op_code, overflow})
		11'b0000_xxxxxx_x : state_nxt = 1;
		11'b0001_100011_x,
		11'b0001_101011_x	: state_nxt = 2;
		11'b0001_000000_x : state_nxt = 6;
		11'b0001_000100_x : state_nxt = 8;
		11'b0001_000010_x : state_nxt = 9;
		
		// addi
		11'b0001_001000_x : state_nxt = 12;
		11'b1100_001000_x : state_nxt = 13;
		11'b1101_001000_x : state_nxt = 0;
		
		11'b0010_100011_x : state_nxt = 3;
		11'b0010_101011_x : state_nxt = 5;
		11'b0011_xxxxxx_x : state_nxt = 4;
		11'b0100_xxxxxx_x : state_nxt = 0;
		11'b0101_xxxxxx_x : state_nxt = 0;
		11'b0110_xxxxxx_x : state_nxt = 7;
		11'b0111_xxxxxx_0 : state_nxt = 0;
		11'b0111_xxxxxx_1 : state_nxt = 11;
		11'b1000_xxxxxx_x : state_nxt = 0;
		11'b1001_xxxxxx_x : state_nxt = 0;

        // exceptions
        11'b0001_xxxxxx_x : state_nxt = 10;
        11'b1010_xxxxxx_x : state_nxt = 0;

        11'b1011_xxxxxx_x : state_nxt = 0;


		default : state_nxt = 0;
	endcase

// set current state_crt
always @(posedge clk)
	if (res == 1)
		state_crt <= 0;
	else
		state_crt <= state_nxt; 

// set outputs		
always @(state_crt)
	case (state_crt)
		// CauseWrite, IntCause, EPCWrite, PCWriteCond, PCWrite, IorD, MemRead, MemWrite, MemtoReg, IRWrite, RegDst, RegWrite, ALUSrcA, ALUSrcB, ALUOp, PCSource 
		4'b0000 : control = 19'b0_x_0_0_1_0_1_0_x_1_x_0_0_01_00_00;
		4'b0001 : control = 19'b0_x_0_0_0_x_0_0_x_0_x_0_0_11_00_xx;
		4'b0010 : control = 19'b0_x_0_0_0_x_0_0_x_0_x_0_1_10_00_xx;
		4'b0011 : control = 19'b0_x_0_0_0_1_1_0_x_0_x_0_x_xx_xx_xx;
		4'b0100 : control = 19'b0_x_0_0_0_x_0_0_1_0_0_1_x_xx_xx_xx;
		4'b0101 : control = 19'b0_x_0_0_0_1_0_1_x_0_x_0_x_xx_xx_xx;
		4'b0110 : control = 19'b0_x_0_0_0_x_0_0_x_0_x_0_1_00_10_xx;
		4'b0111 : control = 19'b0_x_0_0_0_x_0_0_0_0_1_1_x_xx_xx_xx;
		4'b1000 : control = 19'b0_x_0_1_0_x_0_0_x_0_x_0_1_00_01_01;
		4'b1001 : control = 19'b0_x_0_0_1_x_0_0_x_0_x_0_x_xx_xx_10;
		
		// addi
		4'b1100 : control = 19'b0_x_0_0_0_x_0_0_x_0_x_0_1_10_00_xx;
		4'b1101	: control = 19'b0_x_0_0_0_x_0_0_0_0_0_1_x_xx_xx_xx;

        // exceptions
        4'b1010 : control = 19'b1_0_1_0_1_x_x_0_x_x_x_0_0_01_01_11;
        4'b1011 : control = 19'b1_1_1_0_1_x_x_0_x_x_x_0_0_01_01_11;
		
		default : control = 19'b0_x_0_0_0_x_0_0_x_0_x_0_x_xx_xx_xx;
	endcase
endmodule
