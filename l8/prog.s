.text
	addi $t0, $zero, 256
	addi $t1, $zero, 3
	addi $t2, $zero, 0
	
sum_loop:
	lw, $t3, 0($t0)
	add $t2, $t2, $t3
	addi $t0, $t0, 4 
	addi $t1, $t1, -1
	beq $t1, $zero, done
	j sum_loop
	
done:
	sw $t2, 0($t0)

forever:
	j forever

