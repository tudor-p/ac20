module data_path(
	input clk,
	input res,
	
    input CauseWrite,
    input IntCause,
    input EPCWrite,
	input PCWriteCond,
	input PCWrite,
	input IorD,
	input MemRead,
	input MemWrite,
	input MemtoReg,
	input IRWrite,
	input RegDst,
	input RegWrite, 
	input ALUSrcA, 
	input [1:0] ALUSrcB, 
	input [1:0] ALUOp, 
	input [1:0] PCSource,
	
	output [5:0] op_code,
	output reg overflow
);
reg [31:0] EPC;
reg [31:0] CAUSE;

reg [31:0] PC;
reg [31:0] IR;

reg [31:0] ALUout;

reg [31:0] A;

reg [31:0] B;

reg [31:0] MDR;

reg [7:0] mem [0:1023];

reg [31:0] regs [0:31];

wire [31:0] addr_mem;

wire Zero;

wire [31:0] da;

wire [31:0] db;

reg [31:0] alu;

wire [31:0] opA;

wire [31:0] opB;

wire [31:0] ext32;

wire [31:0] ext32sh;

wire [4:0] rc;

assign Zero = (alu == 0) ? 1 : 0;

assign opA = (ALUSrcA == 1) ? A : PC;

assign opB = (ALUSrcB == 2'b00) ? 	B :
			 ((ALUSrcB == 2'b01) ? 	4 :
			 ((ALUSrcB == 2'b10) ? 	ext32 : 
									ext32sh ));

// assign ext32 = {16{IR[15]}, IR[15:0]}; // ??? err 

// workaroud for above error
assign ext32[31:16] = {16{IR[15]}};
assign ext32[15:0] = IR[15:0];

assign ext32sh = {ext32[29:0], 2'b00}; // ext32<<2

always@(ALUOp or IR[5:0] or opA or opB)
	casex({ALUOp, IR[5:0]})
		8'b00_xxxxxx : alu = opA + opB;
		8'b01_xxxxxx : alu = opA - opB;
		8'b10_100000 : alu = opA + opB;
		8'b10_100010 : alu = opA - opB;
		8'b10_100100 : alu = opA & opB;
		8'b10_100111 : alu = ~(opA | opB);
		8'b10_100101 : alu = opA | opB;
		default: alu = 32'bz; // !!! 
	endcase

// actualizare PC
always@(posedge clk)
	if (res == 1) 
		PC <= 0;
	else
		casex({PCWrite, PCWriteCond, Zero, PCSource})
			5'b1_x_x_00: PC <= alu;
			5'b0_1_1_xx: PC <= ALUout;
			5'b1_x_x_10: PC <= {PC[31:28], IR[25:0], 2'b00};
            5'b1_x_x_11: PC <= 128;
		endcase

always@(posedge clk)
	casex({res, IRWrite})
		2'b1_x : IR <= 0;
		2'b0_1 : begin 
			IR[31:24] 	<= mem[addr_mem+3];
			IR[23:16] 	<= mem[addr_mem+2];
			IR[15:8] 	<= mem[addr_mem+1];
			IR[7:0] 	<= mem[addr_mem];
			end
	endcase

assign addr_mem = (IorD == 0) ? PC : ALUout;

always@(posedge clk)
	ALUout <= alu;

always@(posedge clk)
	A <= da;

always@(posedge clk)
	B <= db;

always@(posedge clk) begin
	MDR[31:24] 	<= mem[addr_mem+3];
	MDR[23:16] 	<= mem[addr_mem+2];
	MDR[15:8] 	<= mem[addr_mem+1];
	MDR[7:0] 	<= mem[addr_mem];	
end

wire [4:0] ra = IR[25:21];

assign da = regs[ra];

wire [4:0] rb = IR[20:16];

assign db = regs[rb];	

assign rc = (RegDst == 1) ? IR[15:11] : IR[20:16];

always@(posedge clk)
	if (res == 1) begin
		regs[0] <= 0; regs[1] <= 0; regs[2] <= 0; regs[3] <= 0; regs[4] <= 0; regs[5] <= 0;
		regs[6] <= 0; regs[7] <= 0; regs[8] <= 0; regs[9] <= 0; regs[10] <= 0; regs[11] <= 0;
		regs[12] <= 0; regs[13] <= 0; regs[14] <= 0; regs[15] <= 0; regs[16] <= 0; regs[17] <= 0;
		regs[18] <= 0; regs[19] <= 0; regs[20] <= 0; regs[21] <= 0; regs[22] <= 0; regs[23] <= 0;
		regs[24] <= 0; regs[25] <= 0; regs[26] <= 0; regs[27] <= 0; regs[28] <= 0; regs[29] <= 0;
		regs[30] <= 0; regs[31] <= 0;
	end else if (rc != 0 && RegWrite == 1) 
		regs[rc] <= (MemtoReg == 1) ? MDR : ALUout; 

always@(posedge clk)
	if (MemWrite == 1) begin
		mem[addr_mem+3]	<= B[31:24];
		mem[addr_mem+2] <= B[23:16];
		mem[addr_mem+1] <= B[15:8];
		mem[addr_mem+0] <= B[7:0];	
	end
	
assign op_code = IR[31:26];

// exception logic

always@(posedge clk) begin
    if(EPCWrite) EPC <= alu;
    if(CauseWrite) CAUSE <= IntCause?32'b1:32'b0;
end

always@(posedge clk) begin
	overflow<=(op_code==0)&&({ALUOp, IR[5:0]}==8'b10_100000)&&(A[31]==B[31])&&(A[31]!=alu[31]);
end

initial begin
	// mem init with some code....
	// codul incepe la adresa 0 (!= 0x0040000 din QtSpim)
	mem[3] = 8'h20; mem[2] = 8'h08; mem[1] = 8'h01; mem[0] = 8'h00;  
	mem[7] = 8'h20; mem[6] = 8'h09; mem[5] = 8'h00; mem[4] = 8'h03;
	mem[11] = 8'h20; mem[10] = 8'h0a; mem[9] = 8'h00; mem[8] = 8'h00;
	mem[15] = 8'h8d; mem[14] = 8'h0b; mem[13] = 8'h00; mem[12] = 8'h00;
	mem[19] = 8'h01; mem[18] = 8'h4b; mem[17] = 8'h50; mem[16] = 8'h20;
	mem[23] = 8'h21; mem[22] = 8'h08; mem[21] = 8'h00; mem[20] = 8'h04;
	mem[27] = 8'h21; mem[26] = 8'h29; mem[25] = 8'hff; mem[24] = 8'hff;
	mem[31] = 8'h11; mem[30] = 8'h20; mem[29] = 8'h00; mem[28] = 8'h01; // !!!
	mem[35] = 8'h08; mem[34] = 8'h00; mem[33] = 8'h00; mem[32] = 8'h03; // !!!
	mem[39] = 8'had; mem[38] = 8'h0a; mem[37] = 8'h00; mem[36] = 8'h00;
	mem[43] = 8'h08; mem[42] = 8'h00; mem[41] = 8'h00; mem[40] = 8'h0a; // !!!

    //vector exceptie
    mem[131] = 8'h08; mem[130] = 8'h00; mem[129] = 8'h00; mem[128] = 32; // !!!
	
	// datele sunt stocate de la adresa 256
	mem[259] = 8'h7f; mem[258] = 8'hff; mem[257] = 8'hff; mem[256] = 8'h01;
	mem[263] = 8'h7f; mem[262] = 8'hff; mem[261] = 8'hff; mem[260] = 8'h02;
	mem[267] = 8'h00; mem[266] = 8'h00; mem[265] = 8'h00; mem[264] = 8'h03;
			
end

// From Qtspim ...
//[00400000] 20080100  addi $8, $0, 256         ; 2: addi $t0, $zero, 256 
//[00400004] 20090003  addi $9, $0, 3           ; 3: addi $t1, $zero, 3 
//[00400008] 200a0000  addi $10, $0, 0          ; 4: addi $t2, $zero, 0 
//[0040000c] 8d0b0000  lw $11, 0($8)            ; 7: lw, $t3, 0($t0) 
//[00400010] 014b5020  add $10, $10, $11        ; 8: add $t2, $t2, $t3 
//[00400014] 21080004  addi $8, $8, 4           ; 9: addi $t0, $t0, 4 
//[00400018] 2129ffff  addi $9, $9, -1          ; 10: addi $t1, $t1, -1 
//[0040001c] 11200002  beq $9, $0, 8 [done-0x0040001c]; 11: beq $t1, $zero, done 
//[00400020] 08100003  j 0x0040000c [sum_loop]  ; 12: j sum_loop 
//[00400024] ad0a0000  sw $10, 0($8)            ; 15: sw $t2, 0($t0) 
//[00400028] 0810000a  j 0x00400028 [forever]   ; 18: j forever 

endmodule
