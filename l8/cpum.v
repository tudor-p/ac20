module cpum(
	input clk,
	input res
);

wire [5:0] op_code;
	
wire [18:0] control;
wire overflow;
	
data_path DP(
	.clk(clk),
	.res(res),
	
    .CauseWrite(control[18]),
    .IntCause(control[17]),
    .EPCWrite(control[16]),
	.PCWriteCond(control[15]),
	.PCWrite(control[14]),
	.IorD(control[13]),
	.MemRead(control[12]),
	.MemWrite(control[11]),
	.MemtoReg(control[10]),
	.IRWrite(control[9]),
	.RegDst(control[8]),
	.RegWrite(control[7]), 
	.ALUSrcA(control[6]), 
	.ALUSrcB(control[5:4]), 
	.ALUOp(control[3:2]), 
	.PCSource(control[1:0]),

	.op_code(op_code),
    .overflow(overflow)
);

ctrl_fsm  CP(
	clk,
	res,
	op_code,
    overflow,
	control	
);

endmodule
